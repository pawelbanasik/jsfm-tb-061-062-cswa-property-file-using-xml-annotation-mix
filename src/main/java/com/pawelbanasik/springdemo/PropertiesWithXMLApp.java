package com.pawelbanasik.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.pawelbanasik.springdemo.domain.Organization;
import com.pawelbanasik.springdemo.domain.promotion.TradeFair;

public class PropertiesWithXMLApp {
	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		
		Organization organization = (Organization) context.getBean("myorg");
		System.out.println(organization.toString());
		
		((ClassPathXmlApplicationContext) context).close();
		
		
	}
}
